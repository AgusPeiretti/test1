
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Loader from "./components/loader/Loader.jsx";
import { ApiContext } from "./context/ApiContext.jsx";
import Branding from "./pages/branding/Branding.jsx";
import Login from "./pages/login/Login.jsx";
import InicioSesion from "./pages/inicioSesion/InicioSesion.jsx";
import Registro from "./pages/registro/Registro.jsx";
import "./styles/styles.scss";
import Home from "./pages/home/Home.jsx";
import Zonas from "./pages/zonas/Zonas.jsx";
import Categoris from "./pages/categorias/Categoris.jsx";
import Miperfil from "./pages/mi-perfil/Miperfil.jsx";


function App() {
  
  return (
    <div className="mobile-div">
      <ApiContext>
        <Router>
          <Routes>
            <Route path="/" element={<Miperfil/>} />
          </Routes>
        </Router>
      </ApiContext>
    </div>
  );
}

export default App;