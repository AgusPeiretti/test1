import React from "react";
import bar from "../../assets/bars.png";
import { motion } from "framer-motion";
import h1 from "../../assets/res1.png";
import h2 from "../../assets/res2.png";
import h3 from "../../assets/res3.png";
import NavBar from "../../components/navBar/NavBar";

const Home = () => {
  return (
    <div className="home-container">
      <div className="circulo-uno"></div>
      <div className="circulo-dos"></div>
      <div className="top-bar">
        <img src={bar} alt="" />
        <h3>¿Dónde comemo’?</h3>
      </div>
      <div className="saludo-home font-family-primary d-flex-row">
        <p>Bienvenido🎉</p>
      </div>
      <div className="input-div">
        <input type="serch" placeholder="¿Buscás un lugar concreto?" />
      </div>
      <motion.div className="carrousel">
        <motion.div
          drag="x"
          dragConstraints={{ right: 0, left: -360 }}
          className="inner-carrousel"
        >
          <motion.div className="item">
            <img src={h1} alt="" />
          </motion.div>
          <motion.div className="item">
            <img src={h2} alt="" />
          </motion.div>
          <motion.div className="item">
            <img src={h3} alt="" />
          </motion.div>
        </motion.div>
      </motion.div>
      <div className="categorias">
        <button>Restaurantes</button>
        <button>Bares</button>
        <button>Cafes</button>
      </div>
      <div className="nav-bar">
        <NavBar />
      </div>
    </div>
  );
};

export default Home;
