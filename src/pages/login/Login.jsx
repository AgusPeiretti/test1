import React from "react";
import lupa from "../../assets/lupa-loader.png";

const Login = () => {
  return (
    <>
      <div className="login-container d-flex-center d-flex-column">
        <img src={lupa} alt="" />
        <div class="form">
          <form action="">
            <div class="inputBox">
              <input className="campo-imput" type="text" placeholder="Email" />
            </div>
            <div class="inputBox">
              <input
                className="campo-imput"
                type="password"
                placeholder="Contraseña"
              />
            </div>
            <p class="forget font-family-primary">
              ¿Olvidaste tu contraseña?<a href="#"></a>
            </p>
            <div class="botonLogin">
              <input
                className="principal-button"
                type="submit"
                value="Iniciar sesión"
              />
            </div>
          </form>
        </div>
      </div>
    </>
  );
};

export default Login;
