import React from "react";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import NavBar from "../../components/navBar/NavBar";
import camara from "../../assets/camera.png";

let logueado = false;

const Miperfil = () => {
  return (
    <div className="miperfil-container">
      <div className="circulo-uno"></div>
      <div className="top-bar">
        <ArrowBackIcon />
        <h3>Mi Perfil</h3>
      </div>
      <div className="imagen-perfil">
        {logueado ? (
          <div className="img-login">
            <img src="" alt="" />
          </div>
        ) : (
          <div className="no-logueado">
            <img src={camara} alt="imagen-camara" />
          </div>
        )}
      </div>
      <div className="miperfil-categorias">
        <button>Mis guardados</button>
        <button>Mis reseñas</button>
        <button>Mis datos</button>
        <button>FAQs</button>
        <button>Salir</button>
      </div>

      <div className="nav-bar">
        <NavBar />
      </div>
    </div>
  );
};

export default Miperfil;
