import React from "react";
import lupa from "../../assets/lupa-loader.png";

const Registro = () => {
  return (
    <div className="registro-container  d-flex-center d-flex-column">
      <img src={lupa} alt="" />
      <p className="font-family-primary font-w-400 descripcion-registro">
        ¡Registrarte no te llevará más de 1 minuto!
      </p>
      <div className="form d-flex-center d-flex-column">
        <form action="">
          <div>
            <input className="campo-imput" type="text" placeholder="Nombre" />
          </div>
          <div>
            <input className="campo-imput" type="text" placeholder="Apellido" />
          </div>
          <div>
            <input
              className="campo-imput"
              type="password"
              placeholder="Nueva contraseña"
            />
          </div>
          <div>
            <input
              className="campo-imput"
              type="password"
              placeholder="Repetir contraseña"
            />
          </div>
          <div className="button-submit">
            <input
              className="principal-button"
              type="submit"
              value="Registrarme"
            />
          </div>
        </form>
      </div>
    </div>
  );
};

export default Registro;
