import React from "react";
import lupa from "../../assets/lupa-loader.png";
import Loader from "../../components/loader/Loader";

const InicioSesion = () => {
  return (
    <>
      <div className="inicioSesion-container d-flex-center d-flex-column">
        <img src={lupa} alt="" />
        <p className="font-family-primary font-w-400 descripcion-login">
          Para acceder a las mejores recomendaciones, deberás iniciar sesión o
          registrarte
        </p>
        <button className="principal-button">Iniciar sesión</button>
        <button className="principal-button">Registrarme</button>
        {/* <Loader /> */}
      </div>
    </>
  );
};

export default InicioSesion;
