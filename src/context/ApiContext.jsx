import { createContext } from 'react';
import { initializeApp } from 'firebase/app';
import { getFirestore, getDocs, query, collection, where, addDoc, doc, setDoc } from 'firebase/firestore';
import emailjs from '@emailjs/browser';

export const UseApiContext = createContext();

export const ApiContext = ({ children }) => {
    const firebaseConfig = {
        apiKey: "AIzaSyBxUYzhd1ealpnV5S4r2mOt4xKdVWA7-3w",
        authDomain: "donde-comemo.firebaseapp.com",
        projectId: "donde-comemo",
        storageBucket: "donde-comemo.appspot.com",
        messagingSenderId: "463001223250",
        appId: "1:463001223250:web:02ef8a0e44ee6949f2df8b"
      };
    // Initialize Firebase
    const app = initializeApp(firebaseConfig);
    const db = getFirestore(app);


    const searchCollections = async (nameCollection) => {
        // LLAMADA SIMPLE PARA OBTENER TODOS LOS DATOS SOBRE CIERTAS COLECCIONES DE LA BASE DE DATOS.
        const collectionsData = await getDocs(query(collection(db, nameCollection)));
        const collections = collectionsData.docs.map((doc) => {
            return { id: doc.id, ...doc.data() };
        });
        return collections;
    };

    const collectionByParam = async (nameCollection, param, type) => {
        // REVISAR EN LA DOCUMENTACION DE FIREBASE COMO LLAMAR DOCUMENTOS CON PARAMETRO WHERE. USAR "param" Y "type".
        const collectionsData = await getDocs(query(collection(db, nameCollection), where()));
        const collections = collectionsData.docs.map((doc) => {
            return { id: doc.id, ...doc.data() };
        });
        return collections;
    };

    const getUser = async(idUser) => {
        // OBTENER UN USUARIO DE FIREBASE 
        const users = await searchCollections("usuarios")
        return(users.filter(user=>user.id===idUser))
    }

    const addUser = async(newUser) => {
        // AGREGAR UN NUEVO USUARIO A LA COLECCION "usuarios" CON SU CORRESPONDIENTE ARRAY.
        const user = await addDoc(collection(db, "usuarios"), newUser);
        return(user)
    }

    // PARAMETROS QUE DEBE TENER EL DOCUMENTO "USER"
    // usuarioEjemplo={
    //     nombreApellido:"",
    //     email:"",
    //     contrasena:"",
    //     favoritos:[],
    // }
    
    const actionToFavs = async(idUser,array)=>{
        // AGREGAR O ELIMINAR UN FAVORITO A CIERTO USUARIO
        const user =  doc(db, 'usuarios', idUser);
        await setDoc(user, { "favoritos": array }, { merge: true });
    }

    const emailJS = async (data)=>{
        // API NECESARIA PARA ENVIAR UN CORREO ELECTRONICO A CIERTO MAIL.

        // ARRAY NECESARIO DE "data"
        // const array= {
        //     nombre:"",
        //     contrasena:"",
        //     toMail:""
        // }

        emailjs.send('service_rkbguuj', 'template_7y8c547', data)
            .then(function(response) {
                console.log(response)
            return(true)
            }, function(error) {
                console.log(error)
            return(false)
        });
    }

    return (
        <UseApiContext.Provider value={{ searchCollections, collectionByParam, getUser, addUser, actionToFavs, emailJS }}>
            {children}
        </UseApiContext.Provider>
    );
};
