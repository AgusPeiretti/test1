import React from "react";
import home from "../../assets/logo-home.png";
import zonas from "../../assets/logo-ubicacion.png";
import categorias from "../../assets/logo-cats.png";
import perfil from "../../assets/logo-user.png";

const NavBar = () => {
  return (
    <div className="navBar-container">
      <ul>
        <div>
          <span>
            <img src={home} alt="logo-home" />
          </span>
          <li>Home</li>
        </div>
        <div>
          <span>
            <img src={zonas} alt="logo-home" />
          </span>
          <li>Zonas</li>
        </div>
        <div>
          <span>
            <img src={categorias} alt="logo-home" />
          </span>
          <li>Categorias</li>
        </div>
        <div>
          <span>
            <img src={perfil} alt="logo-home" />
          </span>
          <li>Mi perfil</li>
        </div>
      </ul>
    </div>
  );
};

export default NavBar;
